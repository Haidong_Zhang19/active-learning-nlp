## <center> Brat 使用说明 </center>

#### 0. 介绍
Brat 支持实体、关系、事件抽取的标注。可以标注情感。
优点在标注实体的同时可以进行关系标注，还可以实现二级标注。

#### 1. 安装

- Brat只能在Linux下运行
- 下载[Brat](https://brat.nlplab.org/downloads.html)
- 解压并安装brat
    - tar -xf brat-v1.3_Crunchy_Frog.tar
    - cd brat-v1.3_Crunchy_Frog
    - ./install.sh -u
  
  注意： 需要设置用户名和密码，Brat安装路径不能有中文

- 运行brat
    - 直接运行 python standalone.py
    - 或者后台运行 nohup python -u standalone.py > run.log 2>&1&
  
  打印出“Serving brat at http://0.0.0.0:8001”证明安装成功，
  可通过本地浏览器访问127.0.0.1:8001使用brat，也可局域网计算机访问172.90.90.1100:8001，即Brat IP:8001，来使用
  
- 支持中文标注

    brat本身不支持中文，解决办法是在./server/src/projectconfig.py文件的第163行加上：
    ``` 
        n = re.sub(u'[^a-zA-Z\u4e00-\u9fa5<>,0-9_-]', '_', n)
    ```
    注意：修改完之后，文本内容支持中文，但文件名不能是中文
    
#### 2. 使用

- 标注文件生成
    因为BRAT要求collection中每个txt文件都必须有一个对应的.ann文件运行：
    ```
      find 文件夹名称 -name '*.txt'|sed -e 's|\.txt|.ann|g'|xargs touch
    ```
    注：Linux sed 命令是利用脚本来处理文本文件，sed 可依照脚本的指令来处理、编辑文本文件，Sed 主要用来自动编辑一个或多个文件、简化对文件的反复操作、编写转换程序等。
    


    