#!usr/bin/env python
# coding:utf-8
"""
@File: sampling.py
@Time: 2021/10/5 17:36
@Author: Haidong Zhang
@Contact: haidong_zhang14@yahoo.com
@Description: 
"""
from .uncertainty import ClassifierUncertain
from .uncertainty import SequenceUncertain
import numpy as np


class ClassifierSampling(object):
    @staticmethod
    def least_confidence(classifier, X, n_instances=1, **paras):
        """ Least Confidence """
        uncertainty = ClassifierUncertain.uncertainty(classifier, X, **paras)
        assert len(uncertainty.shape) == 1, f"Error in uncertainty method."
        return np.argsort(uncertainty)[-n_instances:]

    @staticmethod
    def margin_sampling(classifier, X, n_instances=1, **paras):
        """ Margin Sampling """
        uncertainty = ClassifierUncertain.margin(classifier, X, **paras)
        assert len(uncertainty.shape) == 1, f"Error in margin method."
        return np.argsort(uncertainty)[-n_instances:]

    @staticmethod
    def entropy_sampling(classifier, X, n_instances=1, **paras):
        """ Entropy """
        uncertainty = ClassifierUncertain.entropy(classifier, X, **paras)
        assert len(uncertainty.shape) == 1, f"Error in entropy method."
        return np.argsort(uncertainty)[-n_instances:]


class SequenceSampling(object):
    @staticmethod
    def least_confidence():
        pass
