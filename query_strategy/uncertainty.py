#!usr/bin/env python
# coding:utf-8
"""
@File: uncertainty.py
@Time: 2021/10/2 13:03
@Author: Haidong Zhang
@Contact: haidong_zhang14@yahoo.com
@Description: 
"""
from sklearn.exceptions import NotFittedError
import numpy as np
from scipy.stats import entropy


class ClassifierUncertain(object):
    @staticmethod
    def uncertainty(classifier, X, **paras):
        """ Least Confidence """
        try:
            probs = classifier.predict_proba(X, **paras)
            max_prob = np.max(probs, axis=-1)
            return 1 - max_prob
        except NotFittedError:
            return np.zeros(shape=(X.shape[0], ))

    @staticmethod
    def margin(classifier, X, **paras):
        """ Margin Sampling """
        try:
            probs = classifier.predict_proba(X, **paras)
            sorted_probs = np.sort(probs, axis=1)[-2:]
            return sorted_probs[-1] - sorted_probs[0]
        except NotFittedError:
            return np.zeros(shape=(X.shape[0], ))

    @staticmethod
    def entropy(classifier, X, **paras):
        """ Entropy """
        try:
            probs = classifier.predict_proba(X, **paras)
            return entropy(probs)
        except NotFittedError:
            return np.zeros(shape=(X.shape[0], ))


class SequenceUncertain(object):
    @staticmethod
    def uncertainty(classifier, X, **paras):
        """ 不确定性 """
        try:
            pass
        except NotFittedError:
            return np.zeros(shape=(X.shape[0], ))